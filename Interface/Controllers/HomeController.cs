﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidade;
using Controlador.Contratos;

namespace Interface.Controllers
{
    [Export]
    public class HomeController : Controller
    {

        [ImportingConstructor]
        public HomeController()
        {

        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Logar(Usuario item)
        {

            using (UserService.UsuarioClient usuarioService = new UserService.UsuarioClient())
            {
                int id = usuarioService.Logar(item.Email, item.Senha);
                if (id > 0)
                {
                    return RedirectToAction("Lista", "Pedido", new { idUsuario = id });
                }
            }

            return View("Index");
        }

    }
}
