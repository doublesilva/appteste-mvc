﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Interface.Controllers
{
    public class PedidoController : Controller
    {
        //
        // GET: /Pedido/

        public ActionResult Lista(int idUsuario)
        {
            using (UserService.UsuarioClient usuarioService = new UserService.UsuarioClient())
            {
                var user = usuarioService.GetUsuario(idUsuario);
                user.Pedidos = usuarioService.GetPedidos(idUsuario);
                return View(user);
            }
        }

    }
}
