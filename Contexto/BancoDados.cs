﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Entidade;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Contexto
{
    public class SpecFlowDB : DbContext
    {
        public DbSet<Pedido> Pedidos { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public SpecFlowDB()
            : base("SpecFlow")
        {
            if (!Database.Exists("SpecFlow"))
            {
                Database.SetInitializer(new SpecFlowDBInitializer());

            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {            
            // Não cria tabelas com nomes pluralizados das entidades
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }
    }

    public class SpecFlowDBInitializer : DropCreateDatabaseAlways<SpecFlowDB>
    {
        protected override void Seed(SpecFlowDB context)
        {
            var user = new Usuario()
            {                
                Email = "silva.pucrs@gmail.com",
                Nome = "Diego Silva",
                Senha = "102030"
            };
            context.Usuarios.Add(user);

            context.Pedidos.Add(new Pedido()
            {
                DtCriacao = DateTime.Now,
                Nome = "Pedido 1",
                Usuario = user
            });

            //All standards will
            base.Seed(context);
        }
    }

    
}
