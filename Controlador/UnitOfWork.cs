﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Contexto;
using Entidade;

namespace Controlador
{
    public class UnitOfWork : IDisposable
    {
        private SpecFlowDB contexto = new SpecFlowDB();
        private ControladorGenerico<Usuario> usuarioControlador;
        private ControladorGenerico<Pedido> pedidoControlador;

        public ControladorGenerico<Usuario> UsuarioControlador
        {
            get
            {
                if (usuarioControlador == null) usuarioControlador = new ControladorGenerico<Usuario>(contexto);
                return usuarioControlador;
            }
        }

        public ControladorGenerico<Pedido> PedidoControlador
        {
            get
            {
                if (pedidoControlador == null) pedidoControlador = new ControladorGenerico<Pedido>(contexto);
                return pedidoControlador;
            }
        }


        public bool Save()
        {
            try
            {
                contexto.SaveChanges();
                return true;
            }           
            catch (Exception)
            {              
                return false;
            }

        }

        private bool disposed = false;
        protected void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    contexto.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
