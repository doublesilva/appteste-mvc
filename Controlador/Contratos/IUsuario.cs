﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Entidade;


namespace Controlador.Contratos
{
    [ServiceContract]
    public interface IUsuario
    {
        [OperationContract]
        bool Salvar(Usuario usuario);
        [OperationContract]
        int Logar(string email, string senha);
        [OperationContract]
        bool EsqueciSenha(string email);
        [OperationContract]
        Usuario GetUsuario(int idUsuario);
        [OperationContract]
        List<Pedido> GetPedidos(int idUsuario);

    }
}
