﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Entidade
{
    public class Usuario : EntidadeBase
    {

        [Required(AllowEmptyStrings = false, ErrorMessage = "Por favor, informe o seu nome")]
        [Display(Name="Nome")]
        public string Nome { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Por favor, informe um email.")]
        [RegularExpression(".+@.+\\..+", ErrorMessage = "Por favor, informe um email válido!")]
        [Display(Name="Email")]
        public string Email { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Por favor, informe sua senha")]
        [StringLength(50, ErrorMessage = "O {0} deve ter pelo menos {2} caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name="Senha")]
        public string Senha { get; set; }

        [Display(Name="Lista Pedidos")]
        public virtual Pedido[] Pedidos { get; set; }

        public Usuario()
            : base()
        {
            
        }
    }
}
