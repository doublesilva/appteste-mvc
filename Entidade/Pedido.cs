﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidade
{
  
    public class Pedido : EntidadeBase
    {
        public Usuario Usuario { get; set; }
        public string Nome { get; set; }
        public DateTime DtCriacao { get; set; }

        public Pedido()
            : base()
        {

        }

    }
}
