﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidade
{
    public class EntidadeBase
    {
        public int Id { get; set; }     

        public bool Ativo { get; set; }

        public bool Excluido { get; set; }

        public EntidadeBase()
        {
            this.Id = 0;           
            this.Ativo = true;
            this.Excluido = false; 

        }
    }
}
