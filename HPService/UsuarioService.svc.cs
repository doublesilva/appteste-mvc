﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Controlador.Contratos;
using Controlador;

namespace HPService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Autenticar" in code, svc and config file together.
    public class UsuarioService : IUsuario
    {
        

        public bool Salvar(Entidade.Usuario usuario)
        {
            using (UnitOfWork unit = new UnitOfWork())
            {
                if (usuario.Id > 0)
                {
                    if (unit.UsuarioControlador.Get(u => u.Email == usuario.Email).Count() > 0)
                    {
                        throw new Exception("Email já existe!");
                    }
                    unit.UsuarioControlador.Update(usuario);
                }
                else unit.UsuarioControlador.Insert(usuario);
                return unit.Save();
            }
        }

        public int Logar(string email, string senha)
        {
            using (UnitOfWork unit = new UnitOfWork())
            {
                var user = unit.UsuarioControlador.Get(u => u.Email == email && u.Senha == senha).FirstOrDefault();
                if (user == null || !user.Ativo || user.Excluido)
                    return 0;
                return user.Id;
            }
        }

        public bool EsqueciSenha(string email)
        {
            using (UnitOfWork unit = new UnitOfWork())
            {
                var user = unit.UsuarioControlador.Get(u => u.Email == email).FirstOrDefault();
                if (user == null || !user.Ativo || user.Excluido)
                    return false;
                return true;
            }
        }


        public List<Entidade.Pedido> GetPedidos(int idUsuario)
        {
            using (UnitOfWork unit = new UnitOfWork())
            {
                return unit.PedidoControlador.Get(p => p.Usuario.Id == idUsuario).ToList();
            }
        }




        public Entidade.Usuario GetUsuario(int idUsuario)
        {
            using (UnitOfWork unit = new UnitOfWork())
            {
                var item =  unit.UsuarioControlador.Get(p => p.Id == idUsuario).FirstOrDefault();
                return item;
            }
        }
    }
}
